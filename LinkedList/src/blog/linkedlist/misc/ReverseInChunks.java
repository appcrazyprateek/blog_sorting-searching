package blog.linkedlist.misc;

class LNode {

	public LNode prev;
	public int data;
	public LNode next;

	public LNode(int data)		{
		this.data=data;
	}

	public String toString()		{
		return ""+data;
	}
}

public class ReverseInChunks {
	public LNode reverseInchunks(LNode root, int k){

		if(root==null)
			return null;

		LNode first=root;
		LNode current=first;

		int count = k-1;
		while(current.next!=null && count > 0)
		{
			current=current.next;
			count--;
		}

		LNode  last=reverseInchunks(current.next,k);
		current.next=null;
		
		LNode subHead=reverse(first);

		first.next=last;

		return subHead;
	}

	private LNode  reverse(LNode head) {

		LNode result;
		if(head.next ==null) 
			return head;

		result = reverse(head.next);

		head.next.next = head;

		head.next = null;

		return result;
	}

	public void displayList(LNode root)
	{

		LNode temp=root;
		while(temp!=null)
		{
			System.out.print(temp.data+"-->");
			temp=temp.next;
		}
		System.out.print("null");
		System.out.println();
	}

	public LNode reverseChunkIterative(LNode head, int k){

		if(head==null)
			return null;
		
		LNode first, previous,last;
		first= previous=last=null;
	
		LNode current=head;
		
		while(current!=null){
			

			LNode temp=current; 
			previous =null;
            LNode next=null;
            int count =k;
            
			while(current!=null  && count>0){
				next = current.next;
				current.next = previous ;
				previous = current ;
				current = next;
				count--;
			}
			
			if(first == null)
				first = previous;
			else
				last.next=previous;

			last=temp;
		}
		return first;
	}



	public static void main(String[] args) {
		LNode head= new LNode(1);
		head.next= new LNode(2);
		head.next.next= new LNode(3);
		head.next.next.next= new LNode(4);
		head.next.next.next.next= new LNode(5);
		head.next.next.next.next.next= new LNode(6);
		head.next.next.next.next.next.next= new LNode(7);
		//head.next.next.next.next.next.next.next= new LNode(8);

		ReverseInChunks obj=new ReverseInChunks();
		obj.displayList(head);
		LNode r=obj.reverseInchunks(head, 3);
		//LNode r=obj.reverseChunkIterative(head, 3);
		obj.displayList(r);

	}
}
